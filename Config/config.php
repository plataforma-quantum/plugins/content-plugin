<?php

return [

    /**
     * Plugin name
     *
     */
    'name' => 'Content',

    /**
     * Plugin services
     *
     */
    'services' => [
        'content_type' => Plugins\Content\Services\ContentTypeService::class,
        'media_type' => Plugins\Content\Services\MediaTypeService::class,
        'category' => Plugins\Content\Services\CategoryService::class,
        'content' => Plugins\Content\Services\ContentService::class,
        'media' => Plugins\Content\Services\MediaService::class,
    ]
];
