<?php

namespace Plugins\Content\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CategoriesController extends Controller
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Categories';

    /**
     * Content Type Service Instance
     *
     */
    protected $contentTypeService;

    /**
     * Midia Type Service Instance
     *
     */
    protected $mediaTypeService;

    /**
     * Category Service Instance
     *
     */
    protected $categoryService;

    /**
     * Selected content type
     *
     */
    protected $contentType;

    /**
     * Constructor method
     *
     */
    public function __construct()
    {

        // Load services
        $this->contentTypeService = _q('content')->service('content_type');
        $this->mediaTypeService = _q('content')->service('media_type');
        $this->categoryService = _q('content')->service('category');

        // Get content type
        if (request()->contentType) {
            $this->contentType = $this->contentTypeService->findFirstByPermalink(request()->contentType);
        }
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid($this->categoryService->getModel());

        $grid->model()->where('content_type_id', $this->contentType->id);

        $grid->column('id', __('Código'))->sortable();
        $grid->column('parent.title', __('Categoria'));
        $grid->column('title', __('Título'));
        $grid->column('permalink', __('Link Permanente'));
        $grid->column('created_at', __('Criado'));
        $grid->column('updated_at', __('Atualizado'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show($this->categoryService->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('title', __('Título'));
        $show->field('permalink', __('Link Permanente'));
        $show->field('created_at', __('Criado'));
        $show->field('updated_at', __('Atualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form($this->categoryService->getModel());

        // Get form data
        $categories = $this->contentType->categories;
        $mediaTypes = $this->mediaTypeService->findAll();

        // Check if there is any media
        $form->submitted(function (Form $form) {
            if (!optional(request()->media)['path']) {
                $form->ignore('media.media_type_id');
            }
        });

        // Creates the form
        $form->hidden('content_type_id')->value($this->contentType->id);
        $form->text('title', __('Título'))->required();
        $form->select('parent_id', __('Categoria'))
            ->options($categories->pluck('title', 'id'))
            ->help(__('Categoria pai dessa categoria'));
        $form->select('media.media_type_id', __('Tipo de Mídia'))->options($mediaTypes->pluck('title', 'id'));
        $form->file('media.path', __('Mídia'));
        $form->ckeditor('body', __('Descrição'));

        return $form;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(__('Categorias'))
            ->description(__('Listagem'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($contentType, $id, Content $content)
    {
        return $content
            ->header(__('Categorias'))
            ->description(__('Exibir'))
            ->body($this->detail($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(__('Categorias'))
            ->description(__('Criar'))
            ->body($this->form());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($contentType, $id, Content $content)
    {
        return $content
            ->header(__('Categorias'))
            ->description(__('Editar'))
            ->body($this->form()->edit($id));
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update($contentType, $id)
    {
        return $this->form()->update($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        return $this->form()->store();
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($contentType, $id)
    {
        return $this->form()->destroy($id);
    }
}
