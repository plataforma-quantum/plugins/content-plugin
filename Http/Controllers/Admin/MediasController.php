<?php

namespace Plugins\Content\Http\Controllers\Admin;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MediasController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Mídias';

    /**
     * Media Type Service Instance
     *
     */
    protected $mediaTypeService;

    /**
     * Media Service Instance
     *
     */
    protected $mediaService;

    /**
     * Constructor method
     *
     */
    public function __construct()
    {
        $this->mediaTypeService = _q('content')->service('media_type');
        $this->mediaService = _q('content')->service('media');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid($this->mediaService->getModel());

        $grid->column('id', __('Código'))->sortable();
        $grid->column('title', __('Título'));
        $grid->column('permalink', __('Link Permanente'));
        $grid->column('mediaType.title', __('Tipo'));
        $grid->column('created_at', __('Criado'));
        $grid->column('updated_at', __('Atualizado'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show($this->mediaService->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('title', __('Título'));
        $show->field('permalink', __('Link Permanente'));
        $show->field('created_at', __('Criado'));
        $show->field('updated_at', __('Atualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $mediaTypes = $this->mediaTypeService->findAll();

        $form = new Form($this->mediaService->getModel());

        $form->select('media_type_id', __('Tipo de Mídia'))->options($mediaTypes->pluck('title', 'id'))->required();
        $form->text('title', __('Título'))->required();
        $form->file('path', __('Arquivo'))->required();

        return $form;
    }
}
