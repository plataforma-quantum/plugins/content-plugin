<?php

namespace Plugins\Content\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ContentsController extends Controller
{

    /**
     * Content Type Service Instance
     *
     */
    protected $contentTypeService;

    /**
     * Midia Type Service Instance
     *
     */
    protected $mediaTypeService;

    /**
     * Category Service Instance
     *
     */
    protected $categoryService;

    /**
     * Content Service Instance
     *
     */
    protected $contentService;

    /**
     * Selected content type
     *
     */
    protected $contentType;

    /**
     * Constructor method
     *
     */
    public function __construct()
    {

        // Load services
        $this->contentTypeService = _q('content')->service('content_type');
        $this->mediaTypeService = _q('content')->service('media_type');
        $this->categoryService = _q('content')->service('category');
        $this->contentService = _q('content')->service('content');

        // Get content type
        if (request()->contentType) {
            $this->contentType = $this->contentTypeService->findFirstByPermalink(request()->contentType);
        }
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid($this->contentService->getModel());

        $grid->model()->where('content_type_id', $this->contentType->id);

        $grid->column('id', __('Código'))->sortable();
        $grid->column('title', __('Título'));
        $grid->column('categories', __('Categorias'))->display(function ($categories) {
            return join(', ', collect($categories)->pluck('title')->toArray());
        });
        $grid->column('permalink', __('Link Permanente'));
        $grid->column('status', __('Status'))
            ->using(['draft' => 'Rascunho', 'published' => 'Publicado'])
            ->label([
                'draft' => 'default',
                'published' => 'success'
            ]);
        $grid->column('created_at', __('Criado'));
        $grid->column('updated_at', __('Atualizado'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show($this->contentService->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('title', __('Título'));
        $show->field('permalink', __('Link Permanente'));
        $show->field('created_at', __('Criado'));
        $show->field('updated_at', __('Atualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form($this->contentService->getModel());

        // Get form data
        $categories = $this->contentType->categories;

        // Creates the form
        $form->hidden('content_type_id')->value($this->contentType->id);
        $form->text('title', __('Título'))->required();
        $form->multipleSelect('categories', __('Categorias'))
            ->options($categories->pluck('title', 'id'))
            ->help(__('Categorias deste conteúdo'));

        // Set content metadata
        $form->embeds('metadata', 'Dados', function ($form) {
            foreach ($this->contentType->metadata as $field) {
                $form->text($field['name'], $field['label'])->rules($field['validation_rules']);
            }
        });

        $form->ckeditor('body', __('Descrição'));
        $form->select('status', 'Status')->options([
            'draft' => 'Rascunho',
            'published' =>  'Publicado'
        ])->required();

        $mediaTypes = $this->mediaTypeService->findAll();
        $form->hasMany('medias', function ($form) use ($mediaTypes) {
            $form->select('media_type_id', 'Tipo de mídia')->options($mediaTypes->pluck('title', 'id'));
            $form->file('path', 'Mídia');
        });

        return $form;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(__('Conteúdo'))
            ->description(__('Listagem'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($contentType, $id, Content $content)
    {
        return $content
            ->header(__('Conteúdo'))
            ->description(__('Exibir'))
            ->body($this->detail($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(__('Conteúdo'))
            ->description(__('Criar'))
            ->body($this->form());
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($contentType, $id, Content $content)
    {
        return $content
            ->header(__('Conteúdo'))
            ->description(__('Editar'))
            ->body($this->form()->edit($id));
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update($contentType, $id)
    {
        return $this->form()->update($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        return $this->form()->store();
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($contentType, $id)
    {
        return $this->form()->destroy($id);
    }
}
