<?php

namespace Plugins\Content\Http\Controllers\Admin;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ContentTypesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Tipos de Conteúdo';

    /**
     * Content Type Service Instance
     *
     */
    protected $contentTypeService;

    /**
     * Constructor method
     *
     */
    public function __construct()
    {
        $this->contentTypeService = _q('content')->service('content_type');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid($this->contentTypeService->getModel());

        $grid->column('id', __('Código'))->sortable();
        $grid->column('icon', __('Ícone'))->display(function ($value) {
            return sprintf('<i class="fa %s"></i>', $value);
        });
        $grid->column('title', __('Título'));
        $grid->column('permalink', __('Link Permanente'));
        $grid->column('created_at', __('Criado'));
        $grid->column('updated_at', __('Atualizado'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show($this->contentTypeService->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('title', __('Título'));
        $show->field('permalink', __('Link Permanente'));
        $show->field('created_at', __('Criado'));
        $show->field('updated_at', __('Atualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form($this->contentTypeService->getModel());

        $form->icon('icon', 'Ícone')->required();
        $form->text('title', 'Título')->required();
        $form->table('metadata', function ($table) {
            $table->text('label', __('Label'));
            $table->text('name', __('Nome'));
            $table->multipleSelect('validation_rules', __('Validações'))->options([
                'nullable' => 'Nulo',
                'string'   => 'Texto',
                'numeric'  => 'Número'
            ]);
        });

        return $form;
    }
}
