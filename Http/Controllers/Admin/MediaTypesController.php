<?php

namespace Plugins\Content\Http\Controllers\Admin;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MediaTypesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Tipos de Mídia';

    /**
     * Media Type Service Instance
     *
     */
    protected $mediaTypeService;

    /**
     * Constructor method
     *
     */
    public function __construct()
    {
        $this->mediaTypeService = _q('content')->service('media_type');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid($this->mediaTypeService->getModel());

        $grid->column('id', __('Código'))->sortable();
        $grid->column('icon', __('Ícone'))->display(function ($value) {
            return sprintf('<i class="fa %s"></i>', $value);
        });
        $grid->column('title', __('Título'));
        $grid->column('permalink', __('Link Permanente'));
        $grid->column('created_at', __('Criado'));
        $grid->column('updated_at', __('Atualizado'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show($this->mediaTypeService->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('title', __('Título'));
        $show->field('permalink', __('Link Permanente'));
        $show->field('created_at', __('Criado'));
        $show->field('updated_at', __('Atualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form($this->mediaTypeService->getModel());

        $form->icon('icon', 'Ícone')->required();
        $form->text('title', 'Título')->required();

        return $form;
    }
}
