<?php

namespace Plugins\Content\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->where('plugin', '=', 'content')->delete();
        $parentId = DB::table('admin_menu')->insertGetId([
            'parent_id' => 0,
            'order' => 2,
            'title' => 'Gerenciador de Conteúdo',
            'icon' => 'fa-dashboard',
            'uri' => NULL,
            'permission' => NULL,
            'created_at' => '2020-06-12 16:24:36',
            'updated_at' => '2020-06-12 16:24:45',
            'plugin' => 'content',
        ]);
        DB::table('admin_menu')->insert([
            [
                'parent_id' => $parentId,
                'order' => 1,
                'title' => 'Tipos de Conteúdo',
                'icon' => 'fa-file-text',
                'uri' => 'content/content-type',
                'permission' => NULL,
                'created_at' => '2020-06-12 16:25:46',
                'updated_at' => '2020-06-12 16:25:46',
                'plugin' => 'content',
            ],
            [
                'parent_id' => $parentId,
                'order' => 2,
                'title' => 'Tipos de mídia',
                'icon' => 'fa-image',
                'uri' => 'content/media-type',
                'permission' => NULL,
                'created_at' => '2020-06-12 16:26:11',
                'updated_at' => '2020-06-12 16:26:59',
                'plugin' => 'content',
            ],
            [
                'parent_id' => $parentId,
                'order' => 3,
                'title' => 'Mídias',
                'icon' => 'fa-floppy-o',
                'uri' => 'content/medias',
                'permission' => NULL,
                'created_at' => '2020-06-12 16:26:44',
                'updated_at' => '2020-06-12 16:26:44',
                'plugin' => 'content',
            ]
        ]);
    }
}
