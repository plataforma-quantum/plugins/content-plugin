<?php

namespace Plugins\Content\Database\Seeders;

use Illuminate\Database\Seeder;

class ContentDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminMenuTableSeeder::class);
        $this->call(CmsMediaTypesTableSeeder::class);
    }
}
