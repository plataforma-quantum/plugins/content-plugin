<?php

namespace Plugins\Content\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CmsMediaTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('cms_media_types')->delete();
        DB::table('cms_media_types')->insert(array(
            0 =>
            array(
                'id' => 1,
                'icon' => 'fa-image',
                'title' => 'Imagem',
                'permalink' => 'imagem',
                'created_at' => '2020-06-12 16:31:08',
                'updated_at' => '2020-06-12 16:31:08',
                'deleted_at' => NULL,
            ),
            1 =>
            array(
                'id' => 2,
                'icon' => 'fa-file-video-o',
                'title' => 'Vídeo',
                'permalink' => 'video',
                'created_at' => '2020-06-12 16:48:00',
                'updated_at' => '2020-06-12 16:48:00',
                'deleted_at' => NULL,
            ),
            2 =>
            array(
                'id' => 3,
                'icon' => 'fa-file-pdf-o',
                'title' => 'PDF',
                'permalink' => 'pdf',
                'created_at' => '2020-06-12 16:48:10',
                'updated_at' => '2020-06-12 16:48:10',
                'deleted_at' => NULL,
            ),
            3 =>
            array(
                'id' => 4,
                'icon' => 'fa-file-word-o',
                'title' => 'Documento',
                'permalink' => 'documento',
                'created_at' => '2020-06-12 16:48:38',
                'updated_at' => '2020-06-12 16:48:38',
                'deleted_at' => NULL,
            ),
            4 =>
            array(
                'id' => 5,
                'icon' => 'fa-file-powerpoint-o',
                'title' => 'Power Point',
                'permalink' => 'power-point',
                'created_at' => '2020-06-12 16:48:57',
                'updated_at' => '2020-06-12 16:48:57',
                'deleted_at' => NULL,
            ),
            5 =>
            array(
                'id' => 6,
                'icon' => 'fa-file-audio-o',
                'title' => 'Áudio',
                'permalink' => 'audio',
                'created_at' => '2020-06-12 16:49:08',
                'updated_at' => '2020-06-12 16:49:08',
                'deleted_at' => NULL,
            ),
            6 =>
            array(
                'id' => 7,
                'icon' => 'fa-link',
                'title' => 'Link',
                'permalink' => 'link',
                'created_at' => '2020-06-12 16:49:27',
                'updated_at' => '2020-06-12 16:49:27',
                'deleted_at' => NULL,
            ),
        ));
    }
}
