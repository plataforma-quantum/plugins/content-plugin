<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_medias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('media_type_id')->index();
            $table->bigInteger('mediable_id')->index()->nullable();
            $table->string('mediable_type')->nullable();
            $table->string('title')->nullable();
            $table->string('permalink')->unique();
            $table->string('path');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_medias');
    }
}
