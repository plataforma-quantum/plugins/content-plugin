<?php

namespace Plugins\Content\Services;

use Plugins\Content\Entities\Media;
use Quantum\Models\Service;

class MediaService extends Service
{

    /**
     * Service model class
     *
     */
    protected $model = Media::class;
}
