<?php

namespace Plugins\Content\Services;

use Plugins\Content\Entities\Content;
use Quantum\Models\Service;

class ContentService extends Service
{

    /**
     * Model of service
     *
     */
    protected $model = Content::class;
}
