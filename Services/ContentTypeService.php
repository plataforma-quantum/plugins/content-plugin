<?php

namespace Plugins\Content\Services;

use Plugins\Content\Entities\ContentType;
use Quantum\Models\Service;

class ContentTypeService extends Service
{

    /**
     * Service model
     *
     */
    protected $model = ContentType::class;

    /**
     * Get first category with permalink
     *
     */
    public function findFirstByPermalink(string $permalink)
    {
        return $this->findOneBy('permalink', '=', $permalink);
    }
}
