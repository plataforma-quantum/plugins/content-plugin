<?php

namespace Plugins\Content\Services;

use Plugins\Content\Entities\MediaType;
use Quantum\Models\Service;

class MediaTypeService extends Service
{

    /**
     * Service model
     *
     */
    protected $model = MediaType::class;
}
