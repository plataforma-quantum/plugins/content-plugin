<?php

namespace Plugins\Content\Services;

use Plugins\Content\Entities\Category;
use Quantum\Models\Service;

class CategoryService extends Service
{

    /**
     * Service model class
     *
     */
    protected $model = Category::class;

    /**
     * Get first category with permalink
     *
     */
    public function findFirstByPermalink(string $permalink)
    {
        return $this->findOneBy('permalink', '=', $permalink);
    }
}
