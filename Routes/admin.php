<?php

use Illuminate\Support\Facades\Route;

Route::resource('content-type', 'ContentTypesController');
Route::resource('media-type', 'MediaTypesController');
Route::resource('medias', 'MediasController');

Route::resource('{contentType}/categories', 'CategoriesController', [
    'parameters' => ['{contentType}' => 'param']
]);
Route::resource('{contentType}/contents', 'ContentsController', [
    'parameters' => ['{contentType}' => 'param']
]);
