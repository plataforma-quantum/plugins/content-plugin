<?php

namespace Plugins\Content\Traits;

use Plugins\Content\Entities\Media;

trait HasMedias
{
    /**
     * Midiables entitites
     *
     */
    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * Midiables entitites
     *
     */
    public function media()
    {
        return $this->morphOne(Media::class, 'mediable');
    }
}
