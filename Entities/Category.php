<?php

namespace Plugins\Content\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Plugins\Content\Traits\HasMedias;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

class Category extends Model
{
    use SoftDeletes, HasMedias, HasSlug;

    /**
     * Model name on database
     *
     */
    protected $table = 'cms_categories';

    /**
     * Guarded properties
     *
     */
    protected $guarded = [];

    /**
     * Get the options for generating the slug
     *
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('permalink');
    }

    /**
     * BelongsTo Category
     *
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * HasMany Category
     *
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * BelongsToMany Content
     *
     */
    public function contents()
    {
        return $this->belongsToMany(Content::class, 'cms_categories_contents');
    }
}
