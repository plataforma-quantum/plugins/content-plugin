<?php

namespace Plugins\Content\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

class MediaType extends Model
{
    use SoftDeletes, HasSlug;

    /**
     * Table name on database
     *
     */
    protected $table = "cms_media_types";

    /**
     * Guarded fields
     *
     */
    protected $guarded = [];

    /**
     * Get the options for generating the slug
     *
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('permalink');
    }
}
