<?php

namespace Plugins\Content\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

class Media extends Model
{
    use SoftDeletes, HasSlug;

    /**
     * Table name on database
     *
     */
    protected $table = 'cms_medias';

    /**
     * Guarded properties
     *
     */
    protected $guarded = [];

    /**
     * Get the options for generating the slug
     *
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('permalink');
    }

    /**
     * BelongsTo MediaType
     *
     */
    public function mediaType()
    {
        return $this->belongsTo(MediaType::class, 'media_type_id');
    }

    /**
     * MorphTo
     *
     */
    public function mediable()
    {
        return $this->morphTo();
    }
}
