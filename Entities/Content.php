<?php

namespace Plugins\Content\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Plugins\Content\Traits\HasMedias;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

class Content extends Model
{
    use SoftDeletes, HasMedias, HasSlug;

    /**
     * Model name on database
     *
     */
    protected $table = 'cms_contents';

    /**
     * Guarded properties
     *
     */
    protected $guarded = [];

    /**
     * Model casts
     *
     */
    protected $casts = [
        'metadata' => 'json',
    ];

    /**
     * ofType Model Scope
     *
     */
    public function scopeOfType($query, $type)
    {

        return $query->whereHas('contentType', function (Builder $query) use ($type) {
            return $query->where('permalink', '=', $type);
        });
    }

    /**
     * published Model Scope
     *
     */
    public function scopePublished($query)
    {
        return $query->where('status', 'like', '%published%');
    }

    /**
     * Get the options for generating the slug
     *
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('permalink');
    }

    /**
     * BelongsTo ContentType
     *
     */
    public function contentType()
    {
        return $this->belongsTo(ContentType::class);
    }

    /**
     * BelongsToMany Category
     *
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'cms_categories_contents');
    }
}
