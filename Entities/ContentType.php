<?php

namespace Plugins\Content\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\SlugOptions;
use Spatie\Sluggable\HasSlug;

class ContentType extends Model
{
    use SoftDeletes;
    use HasSlug;

    /**
     * Table name on database
     *
     */
    protected $table = "cms_content_types";

    /**
     * Guarded properties
     *
     */
    protected $guarded = [];

    /**
     * Get the options for generating the slug
     *
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('permalink');
    }

    /**
     * Get Metadata Field
     *
     */
    public function getMetadataAttribute($value)
    {
        return array_values(json_decode($value, true) ?: []);
    }

    /**
     * Set metadata Field
     *
     */
    public function setMetadataAttribute($value)
    {
        $this->attributes['metadata'] = json_encode(array_values($value));
    }

    /**
     * HasMany Category
     *
     */
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    /**
     * HasMany Content
     *
     */
    public function contents()
    {
        return $this->hasMany(Content::class);
    }
}
