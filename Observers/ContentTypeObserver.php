<?php

namespace Plugins\Content\Observers;

use Illuminate\Support\Facades\DB;
use Plugins\Content\Entities\ContentType;

class ContentTypeObserver
{

    /**
     * Listen to "created "event"
     *
     */
    public function created(ContentType $contentType)
    {
        $parentId = DB::table('admin_menu')->insertGetId([
            'parent_id' => 0,
            'order' => 10,
            'title' => $contentType->title,
            'plugin' => 'content',
            'icon' => $contentType->icon,
            'uri' => null,
            'permission' => NULL,
            'created_at' => NULL,
            'updated_at' => '2020-06-01 17:54:28'
        ]);

        DB::table('admin_menu')->insert([
            [
                'parent_id' => $parentId,
                'order' => 1,
                'title' => 'Categorias',
                'plugin' => 'content',
                'icon' => 'fa-sitemap',
                'uri' => 'content/' . $contentType->permalink . '/categories',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:54:28'
            ],
            [
                'parent_id' => $parentId,
                'order' => 2,
                'title' => 'Conteúdo',
                'plugin' => 'content',
                'icon' => 'fa-file-text',
                'uri' => 'content/' . $contentType->permalink . '/contents',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:54:28'
            ]
        ]);
    }
}
